const dotenv = require("dotenv");
dotenv.config();

/** @type {import('next').NextConfig} */
const nextConfig = {
  output: "standalone",
  images: {
    remotePatterns: [
      {
        protocol: "http",
        hostname: "127.0.0.1",
      },
      {
        protocol: "https",
        hostname: "admin.bde-enssat.fr",
      },
    ],
  },
  experimental: {
    esmExternals: "loose",
  },
};

module.exports = nextConfig;
