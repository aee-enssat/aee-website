# AEE-ENSSAT Website

![AEE-ENSSAT](https://img.shields.io/badge/ENSSAT-AEE-blue)
![Next.js](https://img.shields.io/badge/Next.js-14.0.3-brightgreen)
![Strapi](https://img.shields.io/badge/Strapi-4.2.0-purple)
![Docker](https://img.shields.io/badge/Docker-available-blue)

Bienvenue sur le dépôt GitLab du site web de l'Association des Élèves de l'ENSSAT (AEE), accessible à l'adresse [bde-enssat.fr](https://bde-enssat.fr). Ce projet vise à offrir une plateforme moderne et interactive pour les étudiants de l'ENSSAT, regroupant toutes les informations essentielles sur les activités, événements et ressources de l'association.

## Aperçu du Site

![Screenshot du site AEE](https://cdn.bde-enssat.fr/img/1.png)
*Page d'accueil du site de l'AEE*

## Technologies Utilisées

Le projet utilise des technologies web modernes pour offrir une expérience utilisateur fluide et réactive :

- **Next.js** : Utilisé pour la construction de l'interface utilisateur, Next.js permet de gérer efficacement le rendu côté serveur et la navigation dynamique.
- **Strapi** : CMS Headless pour la gestion des contenus dynamiques, facilitant l'administration et la mise à jour du site.

## Configuration et Lancement du Projet

### Prérequis

Assurez-vous d'avoir les éléments suivants installés sur votre machine :

- Node.js (version >= 12.x)
- npm ou yarn

#### Étapes pour exécuter le projet

1. **Cloner le dépôt :**

   ```bash
   git clone https://gitlab.com/aee-enssat/aee-website.git
   ```

2. **Naviguer dans le répertoire du projet :**

   ```bash
   cd aee-website
   ```

3. **Installer les dépendances :**

   ```bash
   npm install
   ```

4. **Configuration du fichier `.env` :**

   Copiez le fichier `.env.example` en `.env` et complétez les variables requises. Voici un exemple des variables que vous devrez définir :

   ```bash
   NEXT_ENV_API_URL=http://localhost:1337
   ```

5. **Lancer le serveur de développement :**

   ```bash
   npm run dev
   ```

   Le site sera alors accessible à l'adresse [http://localhost:3000](http://localhost:3000).

---

### Utilisation de Docker

Une image Docker est également disponible pour exécuter le projet. Vous pouvez la récupérer et l'exécuter avec les commandes suivantes :

```bash
docker pull aee-enssat/aee-website
docker run -p 3000:3000 aee-enssat/aee-website
```

Cela démarrera le serveur sur le port 3000.

---

### Contribution

Pour apporter des modifications ou des améliorations, veuillez contacter le responsable technique et infrastructure de l'AEE actuelle à l'adresse suivante : [informatique@bde-enssat.fr](mailto:informatique@bde-enssat.fr).

---

**Liens Importants :**
- [Dépôt GitLab](https://gitlab.com/aee-enssat/aee-website)
- [Site Web de l'AEE](https://bde-enssat.fr)

Pour toute question ou assistance supplémentaire, n'hésitez pas à contacter les mainteneurs du projet via les issues du dépôt ou par email.
