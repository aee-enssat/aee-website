import type { Config } from "tailwindcss";

const config: Config = {
  content: [
    "./src/app/clubs/*.{js,ts,jsx,tsx,mdx}",
    "./src/app/partners/*.{js,ts,jsx,tsx,mdx}",
    "./src/app/components/*.{js,ts,jsx,tsx,mdx}",
    "./src/app/events/*.{js,ts,jsx,tsx,mdx}",
    "./src/app/members/*.{js,ts,jsx,tsx,mdx}",
    "./src/app/*.{js,ts,jsx,tsx,mdx}",
  ],
  theme: {
    extend: {
      aspectRatio: {
        card: "5/2",
      },
      colors: {
        primary: "#a91b3a",
        secondary: "#ddca1c",
        blacklight: "#131313",
      },
      letterSpacing: {
        chill: "10px",
        big: "40.8px",
      },
      spacing: {
        screen: "100vw",
      },
      keyframes: {
        "sliding-text": {
          from: {
            transform: "translateX(0)",
          },
          to: {
            transform: "translateX(-100%)",
          },
        },
      },
      animation: {
        "event-text": "sliding-text 10s linear infinite",
      },
    },
  },
  plugins: [],
};
export default config;
