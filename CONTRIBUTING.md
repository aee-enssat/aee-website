# Guide de Contribution

Merci de considérer contribuer à notre projet !

## Comment Contribuer

Pour savoir ce qu'il y a à faire, veuillez consulter la [section ISSUE Board](https://gitlab.com/aee-enssat/aee-website/-/boards) de GitLab.

Vous y trouverez toutes les issues et tâches ouvertes qui nécessitent de l'attention. Choisissez une issue qui vous intéresse et n'hésitez pas à commencer à travailler dessus. Si vous avez des questions, n'hésitez pas à nous contacter.
