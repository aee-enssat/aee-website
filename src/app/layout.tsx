import type { Metadata } from 'next';
import { Inter } from 'next/font/google';
import Menu from './components/Menu';
import './globals.css';
import Script from 'next/script';
import { Footer } from './components/Footer';
import ConfigSingleton from './utils/ConfigSingleton';
import FollowUsModal from './components/FollowUsModal';

const inter = Inter({ subsets: ['latin'] });

const config = ConfigSingleton.getInstance();
const baseURL = config.getBaseUrl();

export const metadata: Metadata = {
  title: 'AEE - Association des Élèves de l\'ENSSAT',
  description: 'Bienvenue sur le site officiel de l\'Association des Élèves de l\'ENSSAT (AEE). Découvrez nos événements, nos activités et rejoignez notre communauté étudiante dynamique à Lannion.',
  keywords: 'BDE, ENSSAT, Association des Élèves, événements étudiants, activités étudiantes, Lannion',
  authors: [
    {
      name: 'Association des Élèves de l\'ENSSAT',
      url: 'https://www.bde-enssat.fr'
    }
  ],
  metadataBase: new URL(baseURL),
  openGraph: {
    title: 'AEE - Association des Élèves de l\'ENSSAT',
    description: 'Rejoignez l\'Association des Élèves de l\'ENSSAT et participez à nos événements et activités.',
    images: [
      {
        url: 'https://cdn.bde-enssat.fr/AEE2021-plein.png',
        alt: 'Association des Élèves de l\'ENSSAT',
      }
    ],
    url: 'https://bde-enssat.fr',
    type: 'website'
  },
  twitter: {
    card: 'summary_large_image',
    title: 'AEE - Association des Élèves de l\'ENSSAT',
    description: 'Découvrez les activités et événements de l\'Association des Élèves de l\'ENSSAT.',
    images: ['https://cdn.bde-enssat.fr/AEE2021-plein.png'],
    site: '@aeenssat'
  },
  alternates: {
    canonical: 'https://bde-enssat.fr',
  },
  robots: 'index, follow'
};

export default async function RootLayout({
  children,
}: {
  children: React.ReactNode
}) {
  return (
    <html lang="fr">
      <body className={inter.className}>
        <Menu />
        {children}
        <Footer />
        <FollowUsModal />
        <Script async src={`${config.getUmamiUrl()}/script.js`} data-website-id={config.getUmamiSiteId()} />
      </body>
    </html>
  )
}
