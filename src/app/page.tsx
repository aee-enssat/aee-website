import localFont from 'next/font/local'
import Image from 'next/image'
import ArrowText from './components/ArrowText'
import BentoHome from './components/BentoHome'
import Button from './components/Button'
import EventBanner from './components/EventBanner'
import HomeSection from './components/HomeSection'
import Slideshow from './components/Slideshow'
import { getEvents, getHomeSections, getMembers, getPartenaires, getClubs, getSiteSettings } from './utils/StrapiRequests'
import RedisSingleton from './utils/RedisSingleton'
import { LydiaConnector } from './utils/constants'
import { FaArrowDown, FaCrown, FaArrowRight } from 'react-icons/fa';

export const dynamic = 'force-dynamic';

const mursgothic = localFont({ src: '../fonts/mursgothic-widedark.woff' });

export default async function Home() {
  const members = (await getMembers());
  const events = (await getEvents());
  const partenaires = (await getPartenaires());
  const coffee = parseInt((await RedisSingleton.getInstance().get(LydiaConnector.NUMBER_OF_COFFEE_SELLS_KEY)) ?? "-1");
  const clubs = (await getClubs());;

  const sections = await getHomeSections();

  const siteSettings = await getSiteSettings();

  const data = {
    events: events.slice(-8).map(e => e.name),
    stats: {
      adherents: siteSettings.nombre_adherents,
      membres: members.length,
      events: events.length,
      clubs: clubs.length,
    },
    partners: partenaires,
  }

  return (
    <main>
      <svg className='absolute top-0 left-0 w-32 lg:w-64' viewBox="0 0 224 192" fill="none" xmlns="http://www.w3.org/2000/svg">
      <defs>
      <pattern id="logoPattern" patternUnits="userSpaceOnUse" width="224" height="192">
            <image href="/aee_logo_inversed.png" x="20" y="-20" width="120" height="192" />
          </pattern>
        </defs>
        <circle cx="-15" cy="-47" r="239" fill="#A91B3A" />
        <circle cx="-15" cy="-47" r="239" fill="url(#logoPattern)" />
      </svg>
      <Image className='-z-10 absolute bottom-0' loading='eager' src="/enssat_model.png" priority={true} width={1000} height={1000} alt='enssat' />
      <header className='flex h-screen lg:h-[80vh] flex-col items-center justify-center'>
        <h1 className={`${mursgothic.className} text-8xl lg:text-9xl text-primary tracking-widest lg:translate-x-2 leading-none my-4`}>AEE</h1>
        <h2 className='text-lg font-medium text-blacklight tracking-wide lg:tracking-widest leading-none'>Association des Élèves de l'ENSSAT</h2>
        <Button anchor='discover-anchor' leftIcon={<FaArrowDown />} type='primary' label='Découvrir' link='#' className='my-10' />
      </header>
      <ArrowText />
      <section className='mx-0 lg:mx-32 mb-8' id='discover-anchor'>
        <EventBanner events={data.events} />
        <BentoHome stats={data.stats} coffee_number={coffee} discord={siteSettings.discord} />

        <section className='mr-auto ml-auto w-fit my-8 lg:my-16'>
          <Button tracker="home-cta-adhesion" leftIcon={<FaCrown />} rightIcon={<FaCrown />} className='lg:px-48 py-4' type='primary' label="J'adhère" link={siteSettings.devenir_adherent} target="_blank" />
        </section>

        {sections.map((section, idx) =>
          <section key={idx} className={`${idx !== (sections.length - 1) ? 'mb-8 lg:mb-32' : ''}`}>
            <HomeSection invert={idx % 2 !== 0} title={section.title} description={section.description} />
            <Slideshow invert={idx % 2 !== 0} images={section.images} />
          </section>
        )}
        
        <section className='mr-auto ml-auto w-fit my-8 lg:my-16'>
          <Button tracker="home-cta-alpha" className='lg:px-48 py-4' bold={true} type='secondary' label="Découvre la dernière plaquette alpha !" link={siteSettings.lien_plaquette_alpha} target="_blank"/>
        </section>

        <section className='mt-8 lg:mt-16'>
          <svg xmlns="http://www.w3.org/2000/svg" className='ml-auto mr-auto w-8' viewBox="0 0 40 41" fill="none">
            <path d="M35.8198 4.70654L4.00002 36.5263" stroke="#DDCA1C" strokeWidth="8" strokeLinecap="round" />
            <path d="M35.8198 35.8198L4.00002 4.00002" stroke="#DDCA1C" strokeWidth="8" strokeLinecap="round" />
          </svg>
          <h2 className={`${mursgothic.className} text-3xl text-center uppercase mt-8 lg:mt-16 mb-4 lg:mb-8`}>Nos Partenaires</h2>
          <div className='flex flex-wrap items-center justify-between'>
            {data.partners.map((partners, idx) =>
              <a href={partners.link} target='_blank' rel='noreferrer' key={idx} className="hover:scale-110 transition-transform duration-300 ease-in-out">
                <Image
                  className='aspect-square object-contain object-center rounded-full w-24 m-2 border-primary border-solid border-4'
                  src={partners.logo}
                  width={360}
                  height={360}
                  key={idx}
                  alt='partenaire'
                />
              </a>
            )}
          </div>
          <section className='mr-auto ml-auto w-fit mt-8 lg:mt-16'>
            <Button tracker="home-cta-partenaires" className='lg:px-48 py-4' type='primary'  rightIcon={<FaArrowRight />} label="Découvrir les avantages" link='/partners' />
          </section>
        </section>
      </section>
    </main>
  )
}