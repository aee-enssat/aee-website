'use client'

import { TEventHero } from "@/app/utils/StrapiRequests";
import { MutableRefObject, useEffect, useState } from "react";
import { gsap } from "gsap";

export const useEventSlideshow = (
    eventHeros: TEventHero['events'],
    descriptions: MutableRefObject<HTMLDivElement[]>
): [number, (slideIndex: number) => void, () => void] => {

    const [currentSlide, setCurrentSlide] = useState(0);
    const [isSwitchingOfSlide, setSwitchingSlide] = useState(false);

    const maxSlides = eventHeros.length-1;

    const handleNextSlide = (): void => {
        // if the client is already switching of slide do not switch automaticly !
        if(isSwitchingOfSlide)
            return;
        
        // with an interval and to avoid any unexpected side-effects we have to do like this, thank u react :(
        setCurrentSlide(currSlide => {
            let nextSlide: number = currSlide+1;
            if(nextSlide > maxSlides)
                nextSlide = 0;
            
            switchAnimation(currSlide, nextSlide);
            return nextSlide;
        });
    }

    const setSlide = (newSlide: number): void => {
        setCurrentSlide(newSlide);
        switchAnimation(currentSlide, newSlide);
    }

    const switchAnimation = (oldIndex: number, newIndex: number): void => {
        // prevent from another switch
        setSwitchingSlide(true); 
        // retrieve old and new description from dom
        const oldDescription = descriptions.current[oldIndex];
        const newDescription = descriptions.current[newIndex];
 
        // if it's happend do not switch :)
        if(!oldDescription || !newDescription)
            return;

        // switch image from images list
        gsap.to(".images-list", {
            yPercent: (100 / eventHeros.length) * -newIndex,
            duration: 1.2,
            ease: "circ.inOut"
        });

        const transitionSettings = { ease: "circ.inOut", duration: .5 };
        // transition with old and new description 
        gsap.fromTo(
            oldDescription,
            { yPercent: 0, opacity: 1, },
            {
                yPercent: -10,
                opacity: 0,
                ...transitionSettings,
                onComplete: () => {
                    // when old description transition is completed, then show new description (with a beautiful description)
                    oldDescription.classList.remove("active");
                    newDescription.classList.add("active");

                    gsap.fromTo(
                        newDescription,
                        { yPercent: 20, opacity: 0, },
                        {
                            yPercent: 0,
                            opacity: 1,
                            ...transitionSettings,
                            onComplete: () => {
                                // then once all transitions are finish
                                setSwitchingSlide(false);
                            }
                        },
                    );
                },
            }
        );
    }

    return [currentSlide, setSlide, handleNextSlide];

}