import ConfigSingleton from "./ConfigSingleton";

const config = ConfigSingleton.getInstance();

export namespace LydiaConnector {
  export const NUMBER_OF_COFFEE_SELLS_KEY = `${config.getLydiaConnectorKey()}NUMBER_OF_COFFEE_SELLS`;
}
