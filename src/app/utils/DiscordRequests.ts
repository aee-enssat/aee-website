import RedisSingleton from './RedisSingleton';

const redis = RedisSingleton.getInstance();
const CACHE_TTL = 90000; // 1 jour et 1 heure en secondes

export const getStatsDiscordGuild = async (invite_link: string) => {
  const cacheKey = `discord_${invite_link}`;
  const cachedData = await redis.get(cacheKey);

  if (cachedData) {
   // console.log('Using cached data for Discord stats.');
    return JSON.parse(cachedData);
  }

  try {
    const res = await fetch(
      `https://discord.com/api/v9/invites/${invite_link}?with_counts=true&with_expiration=false`
    );
    
    //console.log('Response status:', res.status);

    if (res.status === 429) {
      console.warn('Rate limit exceeded, returning default value.');
      const defaultData = { approximate_member_count: 0 };
      await redis.set(cacheKey, JSON.stringify(defaultData), 'EX', CACHE_TTL);
      return defaultData;
    }

    if (res.status !== 200) {
      throw new Error(`Failed to fetch Discord stats: ${res.status}`);
    }

    const data = await res.json();
    //console.log('Fetched data from Discord API:', data);
    await redis.set(cacheKey, JSON.stringify(data), 'EX', CACHE_TTL);
    console.log('Data cached in Redis with key:', cacheKey);

    return data;
  } catch (error) {
    console.error('Error fetching Discord stats:', error);
    const defaultData = { approximate_member_count: 0 };
    await redis.set(cacheKey, JSON.stringify(defaultData), 'EX', CACHE_TTL);
    return defaultData;
  }
};
