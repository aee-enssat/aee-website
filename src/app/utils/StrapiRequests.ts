import axios from "axios";
import ConfigSingleton from "./ConfigSingleton";
import { getStatsDiscordGuild } from "./DiscordRequests";

const config = ConfigSingleton.getInstance();
const request = axios.create({
  baseURL: `${config.getStrapiUrl()}/api`,
  headers: {
    Authorization: `Bearer ${config.getStrapiToken()}`,
  },
});

type TMember = {
  id: number;
  name: string;
  role?: string;
  picture?: string;
};

type TBureau = {
  id: number;
  name: string;
  membres: TMember[];
};

export type TSocialLink = "discord" | "linkedin" | "facebook" | "insta" | "messenger" | "x";

export type TClubType = "Loisirs" | "Scientifique" | "Artistique" | "Jeux";

export type TClub = {
  id: number;
  name: string;
  description: string;
  type: TClubType;
  cover?: string;
  link?: string;
  link_type?: TSocialLink;
  discord_users?: number;
  location?: string;
  members?: number;
};

export type TEvent = {
  id: number;
  name: string;
  description: string;
  date: Date;
  club?: TClub;
  place?: string;
  cover?: string;
  isPublic: boolean;
};

export type TPartenaire = {
  id: number;
  name: string;
  description: string;
  link?: string;
  logo: string;
  cover?: string;
  localisation?: string;
};

type HomeSection = {
  title: string;
  description: string;
  images: string[];
};

export type TEventHero = {
  events: {
    id: number;
    title: string;
    logo: string;
    description: string[];
    cover: string;
    link?: string;
  }[];
};

export type TSiteSettings = {
  devenir_adherent: string;
  email_global: string;
  discord: string;
  nombre_adherents: number;
  lien_plaquette_alpha: string;
};

export const getSiteSettings = async (): Promise<TSiteSettings> => {
  try {
    const result = await request.get('/site-setting');

    if (result.status !== 200) {
      console.error('Failed to fetch site settings', result.status);
    }

    // if(result.data.error) {
    //   // will be catch in the try/catch
    //   console.error(`An error as occured: ${result.data.data.error}`);
    // }

    const data = result.data.data.attributes;

    const siteSettings: TSiteSettings = {
      devenir_adherent: data.devenir_adherent,
      email_global: data.email_global,
      discord: data.discord,
      nombre_adherents: parseInt(data.nombre_adherents, 10),
      lien_plaquette_alpha: data.lien_plaquette_alpha,
    };

    return siteSettings;
  } catch (error) {
    const e: Error = error as Error;
    console.error('Error fetching site settings:', e.stack);
    throw error;
  }
};

export const getLatestBureaux = async (): Promise<TBureau[]> => {
  try {
    let result = await request.get("/bureaux?sort[0]=createdAt:desc");
    if (result.status !== 200) return []; // Error while trying to access api

    const lastest_bureau_id = result.data.data[0].id;

    if (lastest_bureau_id < 0) return []; // Sadly no bureau has been found
    result = await request.get(
      `/bureaux/${lastest_bureau_id}?populate[0]=bureau&populate[1]=bureau.membres&populate[2]=bureau.membres.picture`
    );
    if (result.status !== 200) return []; // Error while trying to access bureau[id]

    const bureaux = result.data.data.attributes.bureau;
    return bureaux.map((bureau: any) => {
      return {
        id: bureau.id,
        name: bureau.name,
        membres: bureau.membres.data.map((membre: any) => {
          const m: TMember = {
            id: membre.id,
            name: membre.attributes.name,
            role: membre.attributes.role,
          };
          const raw_url = membre.attributes.picture?.data?.attributes?.url;
          if (raw_url) m.picture = `${config.getExternalStrapiUrl()}${raw_url}`;
          return m;
        }),
      } as TBureau;
    });
  } catch (err) {
    console.error(err);
  }
  return [];
};

export const getMembers = async (): Promise<TMember[]> => {
  try {
    let result = await request.get("/membres");
    if (result.status !== 200) return []; // Error while trying to access api

    const membres = result.data.data;

    return membres.map(
      (membre: any) =>
        ({
          id: membre.id,
          name: membre.attributes.name,
          role: membre.attributes.role,
          picture: membre.attributes.picture,
        } as TMember)
    );
  } catch (err) {
    console.error(err);
  }
  return [];
};

export const getEvents = async (opts?: {
  show?: "past" | "future";
}): Promise<TEvent[]> => {
  try {
    let result;
    const today = new Date().toISOString();
    const oneYearAgo = new Date();
    oneYearAgo.setFullYear(oneYearAgo.getFullYear() - 1);
    const oneYearAgoISOString = oneYearAgo.toISOString();

    if (opts?.show === "future") {
      result = await request.get(
        `/events?populate[0]=club&populate[1]=cover&sort=date:asc&filters[date][$gte]=${today}`
      );
    } else if (opts?.show === "past") {
      result = await request.get(
        `/events?populate[0]=club&populate[1]=cover&sort=date:desc&filters[date][$lte]=${today}&filters[date][$gte]=${oneYearAgoISOString}`
      );
    } else {
      result = await request.get("/events?populate[0]=club&populate[1]=cover");
    }

    if (result.status !== 200) return [];

    const events = result.data.data;
    return events.map(
      (event: any) =>
        ({
          id: event.id,
          name: event.attributes.name,
          description: event.attributes.description,
          club: {
            id: event.attributes.club?.data?.id,
            name: event.attributes.club?.data?.attributes?.name,
            type: event.attributes.club?.data?.attributes?.type,
          },
          date: new Date(event.attributes.date),
          place: event.attributes.place,
          cover: event.attributes.cover?.data?.attributes?.url ? `${config.getExternalStrapiUrl()}${event.attributes.cover.data.attributes.url}` : null,
          isPublic: event.attributes.isPublic,
        } as TEvent)
    );
  } catch (err) {
    console.error(err);
  }
  return [];
};


export const getEventHero = async (): Promise<TEventHero | null> => {
  try {
    const result = await request.get(
      "/event-heroes?populate[events][populate]=logo,cover"
    );

    if (result.status !== 200 || !result.data.data) {
      console.error(`Failed to fetch event hero: ${result.status}`);
      return null;
    }

    const data = result.data.data[0]?.attributes;

    if(!data) {
        console.warn("No event hero found");
        return null;
    }

    const events = data.events.map((event: any) => ({
      id: event.id,
      title: event.title,
      logo: event.logo?.data ? `${config.getExternalStrapiUrl()}${event.logo.data.attributes.url}` : '',
      description: event.description,
      cover: event.cover?.data ? `${config.getExternalStrapiUrl()}${event.cover.data.attributes.url}` : '',
      link: event.link ?? '',
    }));

    return { events };
  } catch (err) {
    console.error(err);
    return null;
  }
};

export const getClubs = async (opts?: {
  type: TClubType;
}): Promise<TClub[]> => {
  try {
    let result;

    if (opts?.type) {
      result = await request.get(
        `/clubs?sort=name:asc&filters[type][$eq]=${opts.type}&populate[0]=cover&populate[1]=links&populate[2]=links.social&pagination[pageSize]=30`
      );
    } else {
      result = await request.get(
        "/clubs?sort=name:asc&populate[0]=cover&populate[1]=links&populate[2]=links.social&pagination[pageSize]=30"
      );
    }

    if (result.status !== 200 || !result.data.data) return []; // Error while trying to access api

    const clubs = result.data.data;

    return await Promise.all(
      clubs.map(async (club: any) => {
        if (!club || !club.attributes) return null; 

        const c: TClub = {
          id: club.id,
          name: club.attributes.name,
          description: club.attributes.description,
          type: club.attributes.type,
          location: club.attributes.localisation,
          members: club.attributes.membres,
        };

        if (club.attributes.cover && club.attributes.cover.data) {
          c.cover = `${config.getExternalStrapiUrl()}${
            club.attributes.cover.data.attributes.formats.small.url
          }`;
        }

        if (club.attributes.links && club.attributes.links.length > 0) {
          const firstLink = club.attributes.links[0];
          c.link = firstLink.link;
          
          if (firstLink.social && firstLink.social.data) {
            c.link_type = firstLink.social.data.attributes.name;
          }

          if (c.link && c.link_type === "discord") {
            const invite_code = c.link.split("/").slice(-1)[0];
            if (invite_code) {
              const stats = await getStatsDiscordGuild(invite_code);
              c.discord_users = stats.approximate_member_count;
            }
          }
        }

        return c;
      })
    );
  } catch (err) {
    console.error(err);
  }
  return [];
};

export const getPartenaires = async (): Promise<TPartenaire[]> => {
  try {
    const result = await request.get(
      "/partenaires?populate[0]=cover&populate[1]=logo"
    );

    if (result.status !== 200 || !result.data.data) return []; // Error while trying to access api

    const partenaires = result.data.data;
    return partenaires.map((partenaire: any) => {
      if (!partenaire || !partenaire.attributes) return null;
      const p: TPartenaire = {
        id: partenaire.id,
        name: partenaire.attributes.name,
        description: partenaire.attributes.description,
        link: partenaire.attributes.link,
        logo: `${config.getExternalStrapiUrl()}${
          partenaire.attributes.logo.data.attributes.url
        }`,
        cover: `${config.getExternalStrapiUrl()}${
          partenaire.attributes.cover.data.attributes.url
        }`,
        localisation: partenaire.attributes.localisation,
      };
      return p;
    });
  } catch (err) {
    console.error(err);
  }
  return [];
};

export const getHomeSections = async (): Promise<HomeSection[]> => {
  try {
    const result = await request.get(
      "/home-sections?populate[0]=pictures&populate[1]=pictures.image"
    );

    if (result.status !== 200) return []; // Error while trying to access api

    const sections = result.data.data;
    return sections.map((section: any) => {
      const s: HomeSection = {
        title: section.attributes.title,
        description: section.attributes.description,
        images: section.attributes.pictures.image.data.map((image: any) => {
          return `${config.getExternalStrapiUrl()}${
            image.attributes.formats.medium.url
          }`;
        }),
      };
      return s;
    });
  } catch (err) {
    console.error(err);
  }
  return [];
};
