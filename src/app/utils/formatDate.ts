import { format } from 'date-fns';

export function formatEventDate(date: Date): string {
  const formattedDate = format(date, 'dd/MM/yyyy');
  const formattedTime = format(date, 'HH:mm');
  
  if (formattedTime === '00:00') {
    return `Le ${formattedDate}`;
  }
  return `Le ${formattedDate} à ${formattedTime}`;
}
