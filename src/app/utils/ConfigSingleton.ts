import dotenv from "dotenv";

type TConfig = {
  redis_host: string;
  redis_port: number;
  strapi_url: string;
  strapi_token: string;
  external_strapi_url: string;
  lydia_connector_key: string;
  umami_url: string;
  umami_site_id: string;
  base_url: string;
};

class ConfigSingleton {
  private static instance: ConfigSingleton;

  private config: TConfig;

  private constructor() {
    dotenv.config();

    this.config = {
      redis_host: process.env.REDIS_HOST ?? "127.0.0.1",
      redis_port: 6379,
      strapi_url: process.env.STRAPI_URL ?? "http://localhost:1337",
      strapi_token: process.env.STRAPI_TOKEN ?? "",
      external_strapi_url: process.env.EXTERNAL_STRAPI_URL ?? "",
      lydia_connector_key:
        process.env.LYDIA_CONNECTOR_KEY ?? "LYDIA_CONNECTOR_",
      umami_url: process.env.UMAMI_URL ?? "",
      umami_site_id: process.env.UMAMI_SITE_ID ?? "",
      base_url: process.env.NEXT_PUBLIC_BASE_URL ?? "http://localhost:3000"
    };

    if (!isNaN(parseInt(process.env.REDIS_PORT ?? ""))) {
      this.config.redis_port = parseInt(process.env.REDIS_PORT!);
    }
  }

  public getRedisHost() {
    return this.config.redis_host;
  }

  public getRedisPort() {
    return this.config.redis_port;
  }

  public getStrapiUrl() {
    return this.config.strapi_url;
  }

  public getStrapiToken() {
    return this.config.strapi_token;
  }

  public getExternalStrapiUrl() {
    return this.config.external_strapi_url;
  }

  public getLydiaConnectorKey() {
    return this.config.lydia_connector_key;
  }

  public getUmamiUrl() {
    return this.config.umami_url;
  }

  public getUmamiSiteId() {
    return this.config.umami_site_id;
  }

  public getBaseUrl() {
    return this.config.base_url;
  }

  public static getInstance() {
    if (!ConfigSingleton.instance) {
      ConfigSingleton.instance = new ConfigSingleton();
    }
    return ConfigSingleton.instance;
  }
}

export default ConfigSingleton;
