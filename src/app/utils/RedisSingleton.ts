import { Redis } from "ioredis";
import ConfigSingleton from "./ConfigSingleton";

class RedisSingleton extends Redis {
  private static instance: Redis;

  private constructor() {
    super({
      host: ConfigSingleton.getInstance().getRedisHost(),
      port: ConfigSingleton.getInstance().getRedisPort(),
    });
  }

  public static getInstance() {
    if (!this.instance) {
      this.instance = new RedisSingleton();
    }
    return this.instance;
  }
}

export default RedisSingleton;
