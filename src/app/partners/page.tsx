import { Metadata } from "next";
import Header from '../components/Header';
import localFont from 'next/font/local';
import CardPartenaire from '../components/CardPartners';
import { getPartenaires } from '../utils/StrapiRequests';
import CallToAction from '../components/CallToAction';

export const metadata: Metadata = {
    title: 'Partenaires - AEE',
    description: 'Découvrez nos partenaires précieux, qui soutiennent les activités et les projets de l\'AEE de l\'ENSSAT. Leur engagement contribue à enrichir l\'expérience des étudiants et à renforcer la communauté de l\'ENSSAT.',
};
export const dynamic = 'force-dynamic';

const mursgothic = localFont({ src: '../../fonts/mursgothic-widedark.woff' });

const getData = async () => {
    const partenaires = await getPartenaires();
    return partenaires;
}

export default async function Partenaires() {
    const partenaires = await getData();

    return (
        <div>
            <Header background title="Les partenaires de l'AEE" />
            <section className='mt-8 ml-auto mr-auto lg:max-w-6xl'>
                <p className='leading-10'>
                    Découvrez nos partenaires précieux, qui soutiennent les activités et les projets de l'AEE de l'ENSSAT. Leur engagement contribue à enrichir l'expérience des étudiants et à renforcer la communauté de l'ENSSAT.
                </p>
                <div className='pt-5 lg:pt-8'>
                    {partenaires.map((partenaire, idx) => (
                        <CardPartenaire key={idx} partenaire={partenaire} invert={idx % 2 !== 0} />
                    ))}
                </div>
                <CallToAction />
            </section>
        </div>
    );
}
