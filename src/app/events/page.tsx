import { Metadata } from "next";
import dynamicImport from 'next/dynamic';
import Card from "../components/Card";
import EventSlideshow from "../components/EventSlideshow";
import Header from "../components/Header";
import { getEvents, getEventHero } from "../utils/StrapiRequests";
import { FaClock } from "react-icons/fa";
import { HiMapPin } from "react-icons/hi2";
import { formatEventDate } from "../utils/formatDate";

export const metadata: Metadata = {
  title: 'Evènements au sein de l\'ENSSAT - AEE',
  description: 'Découvrez les évènements organisés par l\'Association des Élèves de l\'ENSSAT (AEE). Participez à nos activités et rejoignez notre communauté étudiante dynamique à Lannion.'
};

export const dynamic = 'force-dynamic';

const AddToCalendarButtonWrapper = dynamicImport(() => import('../components/AddToCalendarButtonWrapper'), { ssr: false });

export default async function Events() {
  const next_events = await getEvents({
    show: "future"
  });
  const past_events = await getEvents({
    show: "past"
  });

  const eventHerosData = await getEventHero();
  const eventHeros = eventHerosData ? eventHerosData.events : [];

  return (
    <main>
      <Header className="lg:bg-transparent event-custom-header" background={false} />
      <EventSlideshow className="hidden lg:block" eventHeros={eventHeros} />
      <Header className="bg-primary text-white lg:text-black lg:bg-transparent" title="Prochains évènements" showLogo={false} />
      <section className="flex flex-col lg:gap-x-4 gap-y-8 lg:grid lg:grid-cols-3 lg:ml-auto lg:mr-auto pt-5 lg:pt-8 lg:max-w-6xl">
        {next_events.map((event) => (
          <Card key={event.id} title={event.name} description={event.description} addToCalendarButton={<AddToCalendarButtonWrapper event={event} />} cover={event.cover} isPublic={event.isPublic}>
            <div className="inline-flex items-center">
              <FaClock /> <p className="ml-2">{formatEventDate(event.date)}</p>
            </div>
            {event.place && <div className="inline-flex ml-2 items-center">
              <HiMapPin /> <p className="ml-1">{event.place}</p>
            </div>}
          </Card>
        ))}
      </section>
      <Header className="mt-8 bg-primary text-white lg:text-black lg:bg-transparent" title="Évènements passés" showLogo={false} />
      <section className="flex flex-col lg:gap-x-4 gap-y-8 lg:grid lg:grid-cols-3 lg:ml-auto lg:mr-auto pt-5 lg:pt-8 lg:max-w-6xl">
        {past_events.map((event) => (
          <Card key={event.id} title={event.name} description={event.description} cover={event.cover} isPublic={event.isPublic}>
            <div className="inline-flex items-center">
              <FaClock /> <p className="ml-2">{formatEventDate(event.date)}</p>
            </div>
            {event.place && <div className="inline-flex ml-2 items-center">
              <HiMapPin /> <p className="ml-1">{event.place}</p>
            </div>}
          </Card>
        ))}
      </section>
    </main>
  );
}
