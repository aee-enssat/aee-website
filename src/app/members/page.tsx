import { Metadata } from "next"
import { Member } from "../components/Member"
import Header from "../components/Header";
import { getLatestBureaux } from "../utils/StrapiRequests";

export const metadata: Metadata = {
    title: 'Membres du bureau - AEE',
    description: 'Découvrez les membres du bureau de l\'AEE de l\'ENSSAT, qui oeuvrent pour animer la vie étudiante et organiser des événements pour les étudiants de l\'ENSSAT.'
}
export const dynamic = 'force-dynamic';

export default async function Membres() {
    const bureaux = await getLatestBureaux();

    return (
        <main>
            <Header background title="Membres de l'AEE" />
            <section className="flex flex-col lg:grid lg:grid-cols-3 lg:ml-auto lg:mr-auto pt-5 lg:pt-8 lg:max-w-6xl">
                {bureaux.map((bureau, idx) => (
                    <section key={idx}>
                        <p className="py-5 bg-primary text-white my-10 mx-4 rounded-lg sticky lg:relative text-center top-3 font-bold tracking-widest uppercase z-10">{bureau.name}</p>
                        <div>
                            {bureau?.membres.map((member, idx: number) => (
                                <Member name={member.name} picture={member.picture} role={member.role} invert={idx % 2 !== 0} key={idx} />
                            ))}
                        </div>
                    </section>
                ))}
            </section>
        </main>
    )
}
