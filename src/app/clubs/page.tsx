import Header from '../components/Header';
import { Metadata } from 'next';
import localFont from 'next/font/local';
import { TClub, TClubType, getClubs, getSiteSettings } from '../utils/StrapiRequests';
import CardClub from '../components/CardClub';

export const metadata: Metadata = {
    title: 'Clubs - AEE',
    description: 'Découvrez les clubs de l\'ENSSAT, une opportunité passionnante pour les étudiants de s\'engager, d\'explorer leurs centres d\'intérêt, de se développer personnellement et de forger leur avenir au sein d\'une communauté estudiantine dynamique.'
}
export const dynamic = 'force-dynamic';

const mursgothic = localFont({ src: '../../fonts/mursgothic-widedark.woff' });
const categories: TClubType[] = ["Loisirs", "Scientifique", "Artistique", "Jeux"];

const getData = async () => {
    const clubs = await getClubs();
    const data = new Map<string, TClub[]>();
    categories.forEach(cat => {
        data.set(cat, clubs.filter(club => club.type === cat));
    });
    return data;
}

export default async function Clubs() {
    const clubs = await getData();
    const siteSettings = await getSiteSettings();
    
    return (
        <div>
            <Header background title="Les clubs de l'ENSSAT" />
            <section className='mt-8 ml-auto mr-auto lg:max-w-6xl'>
                <p className='leading-10'>Découvrez les clubs variés de l'ENSSAT, une opportunité passionnante pour les étudiants de s'engager, d'explorer leurs centres d'intérêt, de se développer personnellement et de forger leur avenir au sein d'une communauté estudiantine dynamique. Ces clubs offrent une expérience riche en rencontres, en activités, et en opportunités de leadership, permettant aux étudiants de créer des souvenirs inoubliables et de développer des compétences précieuses pour leur parcours académique et professionnel.</p>
                {Array.from(clubs.keys()).map((cat, idx) => (
                    <div className='my-16' key={idx}>
                        <h3 className={`${mursgothic.className} text-3xl text-white py-4 lg:pl-4 w-full text-center bg-primary lg:text-left lg:w-7/12`}>{cat}</h3>
                        <div className='flex flex-col lg:gap-x-4 gap-y-8 lg:grid lg:grid-cols-3 lg:ml-auto lg:mr-auto pt-5 lg:pt-8 lg:max-w-6xl'>
                            {clubs.get(cat)?.map((club, idx) => (
                                <div key={idx}>
                                    <CardClub club={club} discord={siteSettings.discord} grow />
                                </div>
                            ))}
                        </div>
                    </div>
                ))}
            </section>
        </div>
    )
}