'use client';
import Image from "next/image";
import { useState } from 'react';
import { FaSearch } from 'react-icons/fa';
import ImageModal from "./ImageModal";

interface CardProps {
    title: string;
    description: string;
    cover?: string;
    isPublic: boolean;
    children?: React.ReactNode;
    addToCalendarButton?: React.ReactNode;
}

const Card: React.FC<CardProps> = ({ title, description, cover, isPublic, children, addToCalendarButton }) => {
    const [isModalOpen, setIsModalOpen] = useState(false);
    const placeholderCover = "/cover.png";
    const isPlaceholder = !cover;

    const handleModalOpen = () => {
        if (!isPlaceholder) {
            setIsModalOpen(true);
        }
    };

    const handleModalClose = () => {
        setIsModalOpen(false);
    };

    return (
        <article className="flex flex-col w-full bg-white shadow-md rounded-3xl overflow-hidden">
            <div className={`relative ${!isPlaceholder ? 'cursor-pointer' : ''} group`} onClick={handleModalOpen}>
                <Image 
                    src={cover || placeholderCover} 
                    className={`w-full h-32 object-cover transition-transform duration-300 ease-in-out transform ${!isPlaceholder ? 'group-hover:scale-105 group-hover:shadow-lg' : ''}`} 
                    width={300} 
                    height={180} 
                    alt="cover" 
                />
                {!isPlaceholder && (
                    <button
                        className="absolute top-2 right-2 p-2 bg-white rounded-full text-black shadow-md transition-transform duration-300 ease-in-out transform hover:scale-110 hover:shadow-lg">
                        <FaSearch />
                    </button>
                )}
                <div className={`absolute bottom-2 left-2 px-3 py-1 rounded-full text-white text-xs font-bold ${isPublic ? 'bg-green-500' : 'bg-red-500'}`}>
                    {isPublic ? 'Ouvert au public !' : 'Reservé ENSSAT'}
                </div>
            </div>
            <div className="flex flex-col grow mx-4 my-4 justify-between">
                <div>
                    <header className="font-bold text-xl">{title}</header>
                    {children && <section className="inline-flex items-center text-sm text-gray-600 my-2">{children}</section>}
                    <section className="mb-4">{description}</section>
                </div>
                {addToCalendarButton && <div className="mt-2">{addToCalendarButton}</div>}
            </div>
            {isModalOpen && <ImageModal src={cover || ''} alt={`${title} cover`} onClose={handleModalClose} />}
        </article>
    );
}

export default Card;
