type TProps = {
    title: string;
    description: string;
    invert?: boolean
    className?: string
}

import localFont from 'next/font/local';
import Button from './Button';

const mursgothic = localFont({ src: '../../fonts/mursgothic-widedark.woff' })

export const HomeSection = (props: TProps) => {
    return (
        <div className="lg:grid lg:grid-cols-12 lg:grid-rows-5 lg:h-64 lg:gap-4">
            <div className={`${mursgothic.className} ${props.invert === true ? 'lg:order-1 lg:pr-8 lg:text-right' : 'lg:pl-8 lg:text-left'} text-3xl pt-8 lg:text-5xl lg:bg-primary text-primary lg:text-white lg:col-span-4 lg:row-span-5 rounded-t-3xl lg:flex lg:items-center uppercase`}>{props.title}</div>
            <p className={`${props.invert === true && 'lg:text-right'} lg:col-span-8 lg:row-span-5 lg:leading-10 lg:text-lg my-4 lg:mb-auto lg:mt-auto break-words`}>{props.description}</p>
        </div>
    )
}

export default HomeSection;