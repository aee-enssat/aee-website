type TProps = {
    color?: string
    size?: number
}

export default function Arrow(props: TProps) {
    return (
        <svg height={props.size ?? 25} viewBox="0 0 32 32" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M4.40137 4L29.5515 29M29.5515 29V10.685M29.5515 29L11.1265 28.6184" stroke={props.color ?? "#DDCA1C"} strokeWidth="5" strokeLinecap="square" />
        </svg>
    )
}