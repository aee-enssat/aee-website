import localFont from 'next/font/local'
import Image from 'next/image'

const mursgothic = localFont({ src: '../../fonts/mursgothic-widedark.woff' })

type TProps = {
    name: string,
    role?: string,
    picture?: string,
    invert?: boolean
}

export const Member = (props: TProps) => {
    return (
        <section>
            {!props.invert &&
                <div className="flex px-4 mb-2">
                    {props.picture &&
                        <div>
                            <Image src={props.picture ?? ""} alt={props.name} className='aspect-square rounded-xl' height={128} width={128} />
                        </div>
                    }
                    {!props.picture &&
                        <div className='bg-primary grow rounded-xl aspect-square'></div>
                    }
                    <div className='flex flex-col grow'>
                        <div className='text-center flex flex-col items-center justify-around h-2/3 border-2 border-primary rounded-xl ml-2'>
                            <p className={`${mursgothic.className} text-xl`}>{props.name}</p>
                            {props.role && <p>{props.role}</p>}
                        </div>
                    </div>
                </div>
            }
            {props.invert &&
                <div className="flex px-4 -mb-5 -translate-y-8">
                    <div className='flex flex-col grow'>
                        <div className='grow'>
                        </div>
                        <div className='text-center flex flex-col items-center justify-around h-2/3 border-2 border-primary rounded-xl mr-2'>
                            <p className={`${mursgothic.className} text-xl`}>{props.name}</p>
                            {props.role && <p>{props.role}</p>}
                        </div>
                    </div>
                    {props.picture &&
                        <div>
                            <Image src={props.picture ?? ""} alt={props.name} className='aspect-square rounded-xl' height={128} width={128} />
                        </div>
                    }
                    {!props.picture &&
                        <div className='bg-primary grow rounded-xl aspect-square'></div>
                    }
                </div>
            }
        </section>

    )
}