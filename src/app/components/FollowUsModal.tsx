'use client';

import { useState, useEffect } from 'react';
import Image from 'next/image';

const FollowUsModal = () => {
  const [showModal, setShowModal] = useState(false);

  useEffect(() => {
    const timer = setTimeout(() => {
      setShowModal(true);
    }, 10000); // Affiche la modal après 10 secondes

    return () => clearTimeout(timer);
  }, []);

  const handleClose = () => {
    setShowModal(false);
  };

  return (
    <div
      className={`fixed bottom-5 right-5 z-30 bg-white p-4 rounded shadow-lg flex items-center space-x-4 transition-opacity duration-500 ${showModal ? 'opacity-100' : 'opacity-0 pointer-events-none'}`}
      style={{ transitionProperty: 'opacity' }}
    >
      <Image src="/instagram-logo.png" alt="Instagram" width={40} height={40} />
      <div>
        <p>Pensez à nous suivre sur Instagram !</p>
        <a href="https://www.instagram.com/aeenssat/" target="_blank" rel="noopener noreferrer" className="text-blue-500 hover:underline">
          @aeenssat
        </a>
      </div>
      <button onClick={handleClose} className="text-gray-500 hover:text-gray-700">&times;</button>
    </div>
  );
};

export default FollowUsModal;
