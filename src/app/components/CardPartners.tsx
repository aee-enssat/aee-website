import Image from 'next/image';
import { FaMapMarkerAlt, FaArrowRight } from 'react-icons/fa';
import Button from '../components/Button';
import { TPartenaire } from '../utils/StrapiRequests';

interface CardPartnersProps {
  partenaire: TPartenaire;
  invert?: boolean; // pour inverser l'ordre
}

export default function CardPartners({ partenaire, invert = false }: CardPartnersProps) {
  return (
    <div className={`flex flex-col lg:flex-row ${invert ? 'lg:flex-row-reverse' : 'lg:flex-row'} items-center border border-gray-200 rounded-lg shadow-lg duration-300 hover:scale-105 hover:shadow-2xl p-4 mb-8`}>
      
      {/* Section Image */}
      <div className="relative w-full lg:w-1/2 h-48 lg:h-64 mb-4 lg:mb-0">
        {partenaire.cover && (
          <Image
            src={partenaire.cover}
            alt={`Couverture de ${partenaire.name}`}
            fill
            style={{ objectFit: 'cover' }}
            sizes="(max-width: 1024px) 100vw, 50vw"
            className="rounded-lg opacity-80"
          />
        )}
        {partenaire.logo && (
          <div className="absolute top-2 left-2 w-24 h-24 bg-white p-2 rounded-full shadow-lg">
            <Image
              src={partenaire.logo}
              alt={`Logo de ${partenaire.name}`}
              layout="responsive"
              width={64}
              height={64}
              sizes="64px"
              className="rounded-full"
            />
          </div>
        )}
      </div>
      
      {/* Section Texte */}
      <div className={`flex flex-col items-center lg:items-start w-full lg:w-1/2 p-4 ${invert ? 'lg:pl-8' : 'lg:pr-8'}`}>
        <h3 className="text-xl font-bold mb-1">{partenaire.name}</h3>
        {partenaire.localisation && (
          <p className="text-sm text-gray-600 mb-2 flex items-center">
            <FaMapMarkerAlt className="mr-2 text-primary" />
            {partenaire.localisation}
          </p>
        )}
        <p className="text-center lg:text-left mb-4">{partenaire.description}</p>
        {partenaire.link && (
          <Button
            link={partenaire.link}
            label="En savoir plus"
            type="primary"
            target="_blank"
            className="mt-4 w-auto"
            rightIcon={<FaArrowRight />}
          />
        )}
      </div>
    </div>
  );
}
