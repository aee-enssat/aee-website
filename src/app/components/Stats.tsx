'use client'

import { gsap } from 'gsap';
import localFont from 'next/font/local';
import { useLayoutEffect, useState } from 'react';

const mursgothic = localFont({ src: '../../fonts/mursgothic-widedark.woff' })

type TProps = {
    title: string,
    number?: string | number,
    link?: string,
    label?: string,
    filled?: boolean,
    className?: string,
    animate?: boolean,
    delay?: number
};

export default function Stats(props: TProps) {
    const [number, setNumber] = useState(0);

    useLayoutEffect(() => {
        const ctx = gsap.context(() => {
            const count = { number: 0 };
            gsap.to(count, {
                duration: 4,
                delay: props.delay ?? 0,
                ease: "power4",
                number: props.number,
                onUpdate: () => {
                    setNumber(Math.ceil(count.number));
                },
            });
        });
        return () => ctx.revert();
    }, [props.number]);

    return (
        <div className={`${props.className} ${props.filled === true ? 'text-white bg-primary' : ''} flex grow flex-col justify-between border-2 border-primary rounded-3xl py-4 px-3`}>
            <p className={`${mursgothic.className} text-xl mb-4`}>{props.title}</p>
            {props.link &&
                <a href={props.link} className={`${mursgothic.className} ${typeof props.number === 'string' ? 'big-underline' : ''} text-xl`}><span className={`${typeof props.number === 'number' ? 'text-6xl' : 'text-4xl'}  mr-4`}>{(props.animate === true) ? number : props.number}</span>{props.label}</a>
            }
            {!props.link &&
                <p className={`${mursgothic.className} ${typeof props.number === 'string' ? 'big-underline' : ''} text-xl`}><span className={`${typeof props.number === 'number' ? 'text-6xl' : 'text-4xl'}  mr-4`}>{(props.animate === true) ? number : props.number}</span>{props.label}</p>
            }
        </div>
    )
}
