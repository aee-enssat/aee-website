"use client";

import localFont from "next/font/local";
import Image from "next/image";
import Link from "next/link";
import { useState } from 'react'

const mursgothic = localFont({ src: '../../fonts/mursgothic-widedark.woff' });

export default function Header(props: {
    title?: string,
    className?: string,
    image?: string,
    background?: boolean,
    showLogo?: boolean
}) {
    const [isHovered, setIsHovered] = useState(false);

    return (
        <header className={`${mursgothic.className} relative h-32 lg:h-24 ${props.background && 'bg-primary'} ${props.background && 'text-white'} w-full flex items-center ${props.className}`}>
            {props.title && <h1 className="absolute top-1/2 -translate-y-1/2 lg:text-3xl w-full text-center">{props.title}</h1>}
            {props.showLogo !== false && (
            <Link href="/">
                <Image
                    src="/aee_logo.png"
                    alt="AEE Logo"
                    width={60}
                    height={60}
                    className={`absolute left-4 top-1/2 -translate-y-1/2 cursor-pointer transition-transform duration-300 ${isHovered ? 'transform scale-110 rotate-6' : ''}`}
                    onMouseEnter={() => setIsHovered(true)}
                    onMouseLeave={() => setIsHovered(false)}
                />
            </Link>
            )}
        </header>
    )
}