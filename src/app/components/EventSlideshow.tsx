'use client';

import { useEffect, useRef } from 'react';
import Image from 'next/image';
import { TEventHero } from "../utils/StrapiRequests";
import { useEventSlideshow } from '../hooks/events/useEventSlideshow';

interface EventSlideshowProps {
  className?: string;
  eventHeros: TEventHero['events'];
}

const EventSlideshow = ({ className, eventHeros }: EventSlideshowProps) => {

  const AUTOMATIC_SWITCH_TIMEOUT = 5e3; // 5 seconds (may be better with an option in strapi dashboard)

  const initialized = useRef(false)

  // refers to slideshow elements
  const descriptions = useRef<HTMLDivElement[]>([]);

  const [currentSlide, setSlide, handleNextSlide] = useEventSlideshow(eventHeros, descriptions);

  useEffect(() => {
    // due to save mode on react we have to do that
    if(!initialized.current) {
      initialized.current = true

      // slideshow automatic transition
      setInterval(() => {
          handleNextSlide();
      }, AUTOMATIC_SWITCH_TIMEOUT);
    }
  }, [eventHeros]);

  useEffect(() => {
    // show the first descriptions as active when page is loaded
    descriptions.current[0]?.classList.add("active");
  }, [descriptions]);

  return eventHeros.length ? (
    <div className={`${className} relative overflow-y-hidden bg-black`} style={{ clipPath: "polygon(100% 0, 100% 100%, 50% 95%, 0 100%, 0 0)", height: "80vh" }}>
      <div className="z-10 absolute bottom-16 right-0">
        <ul id='event-list' className={`max-w-xs tracking-chill text-white text-xl`}>
          {eventHeros.map((event, idx) => (
            <li onClick={() => setSlide(idx)} className={`p-6 my-2 cursor-pointer ${idx === currentSlide ? "active" : ""}`} key={idx}>
              {event.title}
            </li>
          ))}

          <li className="selector" style={{top: `${(currentSlide/eventHeros.length)*100}%`}} />
        </ul>
      </div>
      {eventHeros.map((event, idx) => (
        <div ref={el => descriptions.current[idx] = el as HTMLDivElement} key={idx} className={`event-info`}>
          <Image alt={event.title} width={500} height={500} loading="lazy" className='w-1/3 mb-4' src={event.logo} />
          <p className="max-w-full text-white mb-8">
            {Array.isArray(event.description) ? event.description.join(' ') : event.description}
          </p>
          {event.link && (
            <a className="rounded-xl cursor-pointer uppercase bg-primary text-white py-4 px-8" href={event.link} target="_blank" rel="noopener noreferrer">
              En savoir plus
            </a>
          )}
        </div>
      ))}
      <div className="images-list">
        {eventHeros.map((event, idx) => (
          <Image loading="lazy" key={idx} alt={event.title} className="background" id={`bg-event-${idx}`} src={event.cover} width={1920} height={1080} />
        ))}
      </div>
    </div>
  ) : <div>Loading...</div>;
};

export default EventSlideshow;
