'use client'

import { useState } from "react"

type TProps = {
    events: String[]
}

export default function EventBanner(props: TProps) {
    const [animationState, setAnimationState] = useState<AnimationPlayState>("running");

    return (
        <div>
            <div className="bg-primary text-white p-4 lg:hidden">
                {props.events.map((event, idx) => (
                    <div className='flex items-center pr-2 my-3' key={idx}>
                        <p className={'tracking-chill font-black text-3xl leading-none text-white uppercase'}>
                            {/* ${idx % 2 ? 'text-white' : 'text-primary drop-shadow-[2px_2px_0px_rgb(255,255,255)]'} */}
                            {event}
                        </p>
                        <svg height={25} viewBox="0 0 32 32" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path id="Vector 1" d="M4.40137 4L29.5515 29M29.5515 29V10.685M29.5515 29L11.1265 28.6184" stroke="#DDCA1C" strokeWidth="5" strokeLinecap="square" />
                        </svg>
                    </div>
                ))}
            </div>
            <div className=" bg-primary text-white p-4 w-full rounded-full whitespace-nowrap overflow-hidden hidden lg:block">
                {props.events.map((_, idx) => (
                    <div className="inline-flex animate-event-text" style={{ animationPlayState: animationState, animationDuration: `${props.events.length * 4}s` }} key={idx}>
                        {
                            props.events.map((event, idx) => (
                                <div onMouseEnter={() => setAnimationState('paused')} onMouseLeave={() => setAnimationState('running')} className='flex items-center pr-2 my-3' key={idx}>
                                    <a href="#" className={`text-white tracking-chill font-black text-3xl leading-none uppercase`}>
                                        {event}
                                    </a>
                                    <svg height={25} viewBox="0 0 32 32" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <path id="Vector 1" d="M4.40137 4L29.5515 29M29.5515 29V10.685M29.5515 29L11.1265 28.6184" stroke="#DDCA1C" strokeWidth="5" strokeLinecap="square" />
                                    </svg>
                                </div>
                            ))
                        }
                    </div>
                ))}
            </div>
        </div>
    )
}
