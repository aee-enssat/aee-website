type ImageModalProps = {
  src: string;
  alt: string;
  onClose: () => void;
};

const ImageModal: React.FC<ImageModalProps> = ({ src, alt, onClose }) => {
  const handleOutsideClick = (e: React.MouseEvent<HTMLDivElement>) => {
    if (e.target === e.currentTarget) {
      onClose();
    }
  };

  return (
    <div
      className="fixed inset-0 flex items-center justify-center bg-black bg-opacity-75 z-50"
      onClick={handleOutsideClick}
    >
      <div className="relative max-w-md max-h-3xl p-2 bg-white rounded-lg">
        <img src={src} alt={alt} className="w-full h-auto" />
        <button
          onClick={onClose}
          className="absolute top-0 right-0 m-4 w-10 h-10 bg-primary rounded-full text-white flex items-center justify-center shadow-md transition-transform duration-300 ease-in-out transform hover:scale-110 hover:shadow-lg">
          X
        </button>
      </div>
    </div>
  );
};

export default ImageModal;
