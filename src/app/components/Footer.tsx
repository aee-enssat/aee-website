import { FaFacebook, FaInstagram, FaLinkedin, FaEnvelope } from 'react-icons/fa';
import { FaXTwitter } from 'react-icons/fa6';

export const Footer = () => {
    return (
        <>
            <div className="w-full mt-10 overflow-hidden leading-[0] bg-primary">
                <svg viewBox="0 0 1440 80" xmlns="http://www.w3.org/2000/svg">
                    <path fill="#ffffff" fillOpacity="1" d="M0,30L48,40C96,50,192,70,288,60C384,50,480,20,576,20C672,20,768,50,864,60C960,70,1056,60,1152,50C1248,40,1344,30,1392,25L1440,20L1440,0L1392,0C1344,0,1248,0,1152,0C1056,0,960,0,864,0C768,0,672,0,576,0C480,0,384,0,288,0C192,0,96,0,48,0L0,0Z"></path>
                </svg>
            </div>
            
            <footer className="lg:px-4 lg:py-8 bg-primary text-white">
                <div className="relative lg:text-center pt-10">
                    <p className="lg:mr-4">
                        © AEE - Association des Élèves de l'ENSSAT
                        | Fait avec ❤️ et disponible sur <a className="underline" href="https://gitlab.com/aee-enssat/" target="_blank" rel="noopener noreferrer">Gitlab</a>
                    </p>
                    <div className="mt-4">
                        <p>6 rue de Kérampont – CS 80518 22305 Lannion</p>
                    </div>
                    <div className="mt-4 flex justify-center space-x-6">
                        <a href="mailto:contact@bde-enssat.fr" aria-label="Email">
                            <FaEnvelope size={32} />
                        </a>   
                        <a href="https://www.facebook.com/BDE.ENSSAT" target="_blank" rel="noopener noreferrer" aria-label="Facebook">
                            <FaFacebook size={32} />
                        </a>
                        <a href="https://x.com/aeenssat" target="_blank" rel="noopener noreferrer" aria-label="Twitter">
                            <FaXTwitter size={32} />
                        </a>
                        <a href="https://www.instagram.com/aeenssat" target="_blank" rel="noopener noreferrer" aria-label="Instagram">
                            <FaInstagram size={32} />
                        </a>
                        <a href="https://www.linkedin.com/company/aee-association-des-eleves-de-l-enssat/" target="_blank" rel="noopener noreferrer" aria-label="LinkedIn">
                            <FaLinkedin size={32} />
                        </a>
                    </div>
                    <div className="text-center mt-2">
                        <a href="https://link.bde-enssat.fr/reglement" target="_blank" className="text-lg text-secondary hover:text-secondary-dark underline">
                            Consultez notre charte
                        </a>
                    </div>
                </div>
            </footer>
        </>
    );
}
