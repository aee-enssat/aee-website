import Image from "next/image";
import Link from "next/link";
import { TClub } from "../utils/StrapiRequests";
import { FaUser, FaMapMarkerAlt } from "react-icons/fa";

export default function CardClub(props: {
    club: TClub,
    grow?: boolean
    discord: string;
}) {
    const link_label = {
        'discord': 'Rejoindre le Discord',
        'undefined': 'Plus d\'informations',
        'website': 'Visiter le site web',
        'messenger': 'Visiter la page ',
        'instagram': 'Visiter le compte Instagram',
        'x': 'Visiter le compte X',
        'whatsapp': 'Rejoindre le groupe WhatsApp',
        'linkedin': 'Visiter le profil LinkedIn', 
    };

    const membersCount = props.club.discord_users ?? props.club.members;

    return (
        <article className={`flex flex-col ${props.grow === true && 'h-full'} w-full bg-white shadow-md rounded-3xl`}>
            {props.club.cover &&
                <Image src={props.club.cover} className="object-cover object-center aspect-card w-full rounded-t-3xl" loading="lazy" width={500} height={500} alt="cover" />
            }
            {
                !props.club.cover && props.grow === true &&
                <div className="aspect-card w-full rounded-t-3xl bg-gray-200" />
            }
            <div className="flex flex-col grow mx-4 my-4 justify-start">
                <header className="font-bold text-xl">{props.club.name}</header>
                <section className="inline-flex items-center text-sm text-gray-600 my-2">
                    {membersCount && (
                        <div className="inline-flex items-center mr-4"><FaUser /> <p className="ml-2">{membersCount} membres</p></div>
                    )}
                    {props.club.location &&
                        <div className="inline-flex items-center"><FaMapMarkerAlt /> <p className="ml-2">{props.club.location}</p></div>
                    }
                </section>
                <section className="mb-4">{props.club.description}</section>
                <Link href={props.club.link ?? props.discord} className={`w-full py-2 ${props.club.link_type ? "bg-" + props.club.link_type : "bg-black"} text-white rounded-xl text-center mt-auto mb-0`}>
                    {link_label[props.club.link_type as keyof typeof link_label ?? "undefined"]}
                </Link>
            </div>
        </article>
    );
}
