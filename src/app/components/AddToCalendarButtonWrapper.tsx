'use client';

import React from 'react';
import { AddToCalendarButton } from 'add-to-calendar-button-react';
import { TEvent } from '../utils/StrapiRequests';
import { format } from 'date-fns';
import { fr } from 'date-fns/locale';

interface AddToCalendarButtonWrapperProps {
  event: TEvent;
}

const AddToCalendarButtonWrapper: React.FC<AddToCalendarButtonWrapperProps> = ({ event }) => {

  const extractDateAndTime = (isoString: string) => {
    const dateObj = new Date(isoString);
    const startDate = format(dateObj, 'yyyy-MM-dd', { locale: fr });
    const startTime = format(dateObj, 'HH:mm', { locale: fr });
    return { startDate, startTime };
  };

  const { startDate, startTime } = extractDateAndTime(event.date.toISOString());

  const organizerName = event.club?.name || 'AEE ENSSAT';
  const organizerEmail = 'contact@bde-enssat.fr';
  const organizer = `${organizerName}|${organizerEmail}`;

  const event_place = event.place ?? 'ENSSAT';

  return (
      <AddToCalendarButton
          name={event.name}
          options={['Apple', 'Google', 'iCal', 'Outlook.com']}
          location={event_place}
          description={event.description}
          startDate={startDate}
          startTime={startTime}
          endTime={startTime}
          timeZone="Europe/Paris"
          label="Ajouter à l'agenda"
          lightMode="dark"
          language="fr"
          buttonStyle="round"
          iCalFileName="enssat_event"
          organizer={organizer}
          listStyle="dropup-static"
      />
  );
};

export default AddToCalendarButtonWrapper;

//source : https://stackblitz.com/edit/react-add-to-calendar-9sfckv?file=index.js