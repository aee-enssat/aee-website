'use client'

import { gsap } from 'gsap'
import { useEffect, useLayoutEffect, useState } from 'react'
import { FaCrown } from "react-icons/fa"
import { FiMenu } from 'react-icons/fi';
import Button from './Button'

import { Questrial } from 'next/font/google'
import Link from 'next/link'

const questrial = Questrial({
    subsets: ['latin'],
    weight: '400'
})

export default function Menu() {
    const [isMenuOpenned, setMenuOpenned] = useState(false)
    
    const links = {
        home: {
            label: 'Accueil',
            path: '/'
        },
        members: {
            label: 'Membres',
            path: '/members'
        },
        events: {
            label: 'Evenements',
            path: '/events'
        },
        clubs: {
            label: 'Clubs',
            path: '/clubs'
        },
        partners: {
            label: 'Partenaires',
            path: '/partners'
        }
    }

    useEffect(() => {
        if (isMenuOpenned) {
            gsap.to('#menu', {
                x: 0
            })
            gsap.fromTo(
                '#menu > ul > li',
                {
                    opacity: 0,
                    x: 40
                },
                {
                    opacity: 1,
                    x: 0,
                    stagger: 0.1
                }
            )
        } else {
            gsap.to('#menu', {
                x: '100%',
                delay: 0.5
            })
            gsap.fromTo(
                '#menu > ul > li',
                {
                    opacity: 1,
                    x: 0
                },
                {
                    opacity: 0,
                    x: 40,
                    stagger: 0.1
                }
            )
        }
    }, [isMenuOpenned])

    useLayoutEffect(() => {
        const menu = document.getElementById('menu')

        const setSize = () => {
            const deviceHeight = window.innerHeight
            menu!.style.height = `${deviceHeight}px`
        }

        setSize()

        window.addEventListener('resize', () => { setSize() })

        return () => { window.removeEventListener('resize', () => { setSize() }) }
    })

    return (
        <div>
            <button data-umami-event="menu-button" aria-label='Bouton du menu' onClick={() => { setMenuOpenned(!isMenuOpenned) }} className='text-white bg-primary p-3 rounded-full text-4xl fixed right-5 top-3 cursor-pointer z-50 transition-transform transform hover:scale-105'><FiMenu /></button>
            <div id="menu" className={`${questrial.className} translate-x-full fixed top-0 right-0 p-5 bg-slate-900 text-white h-screen w-screen lg:w-1/4 flex flex-col flex-wrap items-center justify-center z-40`}>
                <ul className="relative list-none m-0 p-0 mb-auto mt-auto">
                    {Object.values(links).map((link, idx) => (
                        <li className="my-12 uppercase text-3xl hover:text-primary transition-colors" key={idx}><Link href={link.path} onClick={() => { setMenuOpenned(false) }}>{link.label}</Link></li>
                    ))}
                </ul>
                <Button tracker='menu-cta-adhesion' className="transition-transform transform hover:scale-105" leftIcon={<FaCrown />} label="Devenir adhérent" type="primary" link='https://www.helloasso.com/associations/association-des-eleves-de-l-enssat' target="_blank" />
            </div>
        </div>
    )
}
