const CallToAction: React.FC = () => {
    return (
        <div className="mt-12 p-6 bg-primary text-white rounded-lg shadow-md flex flex-col items-center text-center">
            <h2 className="text-2xl font-bold mb-4">Envie de devenir partenaire ?</h2>
            <a 
                href="mailto:contact@bde-enssat.fr"
                className="bg-secondary text-black py-2 px-4 rounded-full hover:bg-yellow-600 transition duration-300">
                Contactez-nous !
            </a>
        </div>
    );
}

export default CallToAction;
