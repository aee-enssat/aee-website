'use client'

import localFont from 'next/font/local';
import Image from "next/image";
import { UAParser } from "ua-parser-js";
import Arrow from "./Arrow";

const mursgothic = localFont({ src: '../../fonts/mursgothic-widedark.woff' })

const parser = UAParser();

const openLydia = () => {
    if (parser.device.vendor === "Apple") {
        window.open("https://apps.apple.com/fr/app/lydia/id575913704", "_lydia");
        umami.track('lydia-apple');
    } else if (parser.device.type === "mobile") {
        window.open("https://play.google.com/store/apps/details?id=com.lydia", "_lydia");
        umami.track('lydia-android');
    } else {
        window.open("https://www.lydia-app.com/", "_lydia");
        umami.track('lydia-pc');
    }
}

export default function LydiaComponent(props: {
    className?: string
}) {
    return (
        <div className={`${props.className} text-white aspect-square rounded-3xl card-lydia cursor-pointer`} onClick={openLydia} >
            <Image width={64} height={64} src={"/lydia.png"} alt='lydia' />
            <p className={`${mursgothic.className} text-xl pl-4 pt-4`}>Payez<br />avec Lydia
                <Arrow color="#FFFFFF" />
            </p>
        </div >
    )
}