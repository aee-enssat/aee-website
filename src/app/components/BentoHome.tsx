import localFont from 'next/font/local';
import Image from "next/image";
import LydiaComponent from "./LydiaComponent";
import Stats from "./Stats";

const mursgothic = localFont({ src: '../../fonts/mursgothic-widedark.woff' });

export default async function BentoHome(props: any) {
    return (
        <section className='grid grid-cols-2 lg:grid-cols-6 grid-rows-5 lg:grid-rows-2 mx-2 gap-2 mb-8 my-4'>
            <Stats className='col-span-2' title="adhérents à l'AEE" number={props.stats.adherents} label='étudiants' animate />
            <Stats className='col-span-1' filled title="membres" number={props.stats.membres} animate delay={2} />
            <div className='hidden lg:flex flex-col col-span-2 row-span-2'>
                <div className='h-16 mb-2 bg-primary rounded-3xl'>
                </div>
                <div className='grow text-white rounded-3xl card'>
                    <Image width={480} height={480} src={"/coffee.jpg"} alt='coffee' />
                    <p className={`${mursgothic.className}`}>Café et viennoiseries<br />en Kfet C et H</p>
                </div>
            </div>
            <Stats className='lg:col-span-1 lg:row-span-1 row-span-2' title='cafés vendus ce mois-ci' number={props.coffee_number} animate delay={1} />
            <Stats className='col-span-1' filled title="évènements prévus" number={props.stats.events} animate delay={3} />
            <div className='block lg:hidden col-span-2 text-white rounded-3xl card'>
                <Image className='aspect-square object-cover' width={480} height={480} src={"/coffee.jpg"} alt='coffee' />
                <p className={`${mursgothic.className}`}>Café et viennoiseries<br />en Kfet C et H</p>
            </div>
            <div className='flex flex-col col-span-2'>
                <Stats className='grow' title="envie de discuter ?" link={props.discord || '#'} number="lien du discord" />
                <div className='lg:flex grow flex-row mt-2'>
                     <Stats className='lg:col-span-1' title="clubs" number={props.stats.clubs} animate delay={3} />
                    <div className='bg-primary rounded-3xl w-1/4 ml-2'></div>
                </div>
            </div>
            <LydiaComponent className='col-span-1' />
            <div className='flex flex-col lg:hidden'>
                <div className='grow bg-primary rounded-3xl mb-2'></div>
                <div className='h-2/3 border-2 border-primary rounded-3xl'></div>
            </div>
        </section>
    )
}