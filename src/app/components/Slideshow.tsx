import Image from "next/image";

export const Slideshow = (props: {
    invert?: boolean,
    images: string[]
}) => {
    return (
        <div className={`lg:bg-primary flex justify-between lg:h-96 items-center ${props.invert === true ? 'rounded-tl-3xl' : 'rounded-tr-3xl'} rounded-b-3xl lg:px-4`}>
            {props.images.map((img, idx) => (
                <Image className={`object-cover object-center aspect-video rounded-3xl ${idx > 0 && 'hidden'} lg:block lg:w-1/3 lg:px-2`} key={idx} src={img} height={256} width={512} alt="" />
            ))}
        </div>
    )
}

export default Slideshow;