'use client'

import { ReactElement, useState } from "react";
import { IconType } from "react-icons";

type TProps = {
    link?: string;
    label: string;
    type: "primary" | "secondary";
    className?: string;
    anchor?: string;
    leftIcon?: ReactElement<IconType>;
    rightIcon?: ReactElement<IconType>;
    tracker?: string;
    target?: string;
    bold?: boolean;
    onClick?: () => void;
};

export default function Button(props: TProps) {
    const [isHovered, setIsHovered] = useState(false);

    const goTo = (e: { preventDefault: () => void; }) => {
        if (props.tracker) {
            umami.track(props.tracker);
        }
        if (!props.anchor) return;
        e.preventDefault();
        const anchor = document.getElementById(props.anchor);
        if (!anchor) return;
        anchor.scrollIntoView({
            behavior: "smooth"
        });
    }

    const handleClick = (e: React.MouseEvent<HTMLAnchorElement, MouseEvent>) => {
        if (props.anchor) {
            goTo(e);
        } else if (props.link) {
            window.location.href = props.link;
        } else if (props.onClick) {
            props.onClick();
        }
    }

    return (
        <a 
            href={props.link || '#'} 
            target={props.target}
            rel={props.target === "_blank" ? "noopener noreferrer" : undefined}
            data-umami-event={props.tracker} 
            onClick={props.link && !props.anchor ? undefined : handleClick}
            onMouseEnter={() => setIsHovered(true)}
            onMouseLeave={() => setIsHovered(false)}
            className={`px-16 py-4 ${props.type === "secondary" ? 'bg-secondary text-blacklight' : 'bg-primary text-white'} cursor-pointer flex items-center font-medium rounded-full ${props.bold ? 'font-bold' : ''} ${isHovered ? 'transform scale-105 shadow-lg' : ''} transition-all duration-300 ${props.className}`}
        >
            {props.leftIcon && <span className="mr-2">{props.leftIcon}</span>}
            {props.label}
            {props.rightIcon && <span className="ml-2">{props.rightIcon}</span>}
        </a>
    )
}
