'use client';

import { useEffect, useState } from 'react';
import Image from 'next/image';

const ArrowText = () => {
  const [showArrow, setShowArrow] = useState(true);
  const [isFading, setIsFading] = useState(false);

  useEffect(() => {
    const handleScroll = () => {
      const offset = window.scrollY;
      const headerHeight = window.innerHeight;
      setShowArrow(offset < headerHeight);
    };

    window.addEventListener('scroll', handleScroll);

    return () => {
      window.removeEventListener('scroll', handleScroll);
    };
  }, []);

  useEffect(() => {
    if (!showArrow) {
      setIsFading(true);
      setTimeout(() => setIsFading(false), 500);
    }
  }, [showArrow]);

  return (
    <div className={`fixed top-16 right-4 z-30 flex items-center ${showArrow ? 'fade-in' : 'fade-out'} ${isFading ? 'fade-out' : ''}`}>
      <span className='bg-[#96275e] text-white p-3 rounded-full mr-2 mt-6'>En découvrir plus !</span>
      <Image src="/arrow.png" alt="Flèche" width={80} height={80} />
    </div>
  );
};

export default ArrowText;
